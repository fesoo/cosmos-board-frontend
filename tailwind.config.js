module.exports = {
  purge: ['./index.html', './src/**/*.{vue,js,ts,jsx,tsx}'],
  darkMode: 'media', // or false or 'class'
  theme: {
    // colors: {},
    extend: {
      colors: {
        current: 'currentColor',
        cosmos: {
          light: '#ff4938',
          DEFAULT: '#ee3126',
          dark: '#ee3126',
          production: '#ff0000',
          marketing: '#00b6c7',
          support: '#625af2',
        },
      },
      animation: {
        'blink': 'blink 2s infinite'
      },
      keyframes: {
        blink: {
          '0%, 49%': { opacity: 1 },
          '50%, 100%': { opacity: 0 },
        }
      }
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
