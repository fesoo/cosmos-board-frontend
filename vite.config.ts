import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [vue()],
  build: {
    brotliSize: false,
    chunkSizeWarningLimit: 800, // Default: 500
  },
})
