# Инфоборд Космоса
Интерактивная панель с множеством информации о компании для сотрудников.

**Адрес API Инфоборда: board.cosmos-web.ru**
- Репозиторий сервера: https://gitlab.cosmos-web.ru/Developers/cosmos-board-backend
- Репозиторий клиента: https://gitlab.cosmos-web.ru/Developers/cosmos-board-frontend

[Макет в Figma](https://www.figma.com/file/mCVQMKsm36IfzSM1aoOsv9/%D0%B8%D0%BD%D1%84%D0%BE%D0%B1%D0%BE%D1%80%D0%B4-4%D0%BA?node-id=0%3A1) / [Техническое Задание](https://docs.google.com/document/d/1ogkwtigEK7V4TImvzGRwy8HkTiBNJIBgmn_YjmmlX6s/edit)

## 🚀 Установка и команды
`npm i` - Установка
`npm run dev` - Режим разработки с HMR (Vite ⚡)
`npm run build` - Сборка проекта

## 🧩 Рекомендации к плагинам
- [Vetur](https://marketplace.visualstudio.com/items?itemName=octref.vetur)
- [Tailwind CSS IntelliSense](https://marketplace.visualstudio.com/items?itemName=bradlc.vscode-tailwindcss)

## 🛠️ Стек разработки проекта:

### Серверная часть:
- Node.js
- Язык: [Typescript](https://www.typescriptlang.org/docs/)
- Web-сервер: [Express](http://expressjs.com/en/4x/api.html)
- Websocket-сервер: [Socket.io](https://socket.io/docs/v4) ([Пример клиент-сервера](https://github.com/socketio/socket.io/tree/master/examples/typescript))
- Под управлением: [PM2](#)

### Клиентская часть:
- Сборка: [Vite](https://vitejs.dev/)
- [Socket.io](https://socket.io/docs/v4) ([Пример клиент-сервера](https://github.com/socketio/socket.io/tree/master/examples/typescript))
- [Vue 3](https://v3.vuejs.org/)
- [Tailwind CSS](https://tailwindcss.com/docs)
- [Swiper](https://swiperjs.com/vue) ([API](https://swiperjs.com/swiper-api#modules))
- [ApexCharts](https://apexcharts.com/docs/)

### Интеграции:
* WeatherApi ([API](https://www.weatherapi.com/docs/#apis-realtime))

## Список ближайших доработок:
- [ ] Подключить пакет для генерации QR: [этот](https://www.npmjs.com/package/awesome-qr) или [этот](https://www.npmjs.com/package/qrcode)
- [ ] Подключить [Model-viewer](https://modelviewer.dev/) с нашим логотипом
- [ ] Подключить на фоне 3D-модель с летающей камерой на [Three.js](https://threejs.org/docs/index.html#manual/en/introduction/Creating-a-scene) ([Пример использования](https://codesandbox.io/s/hidden-lake-3fli6?file=/src/App.vue))
- [ ] Выгрузка/получение данных приложения из инфоблоков:
  * ИБ "API инфоборда: Статистика" (`board_stats`)

# 💼 Сотрудничество
Рад буду лицезреть ваши PR и добавить их в проект. Лишь вместе мы едины!
