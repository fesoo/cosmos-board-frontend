import { fromUnixTime, format } from 'date-fns'
import { ru } from 'date-fns/locale'

// Форматирование даты по формату
export const formatStamp = (pattern: string, stamp?: any): string => {
  let date = null
  if (stamp) date = fromUnixTime(stamp)
  else date = new Date()

  return format(date, pattern, { locale: ru })
}
